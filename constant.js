const DEFAULT_PAGESIZE = 10;
const MAX_PAGESIZE = 5;
const MIN_PAGESIZE = 200;


module.exports = {
    DEFAULT_PAGESIZE: DEFAULT_PAGESIZE,
    MAX_PAGESIZE: MAX_PAGESIZE,
    MIN_PAGESIZE: MIN_PAGESIZE
}